/******************************************************************************
 * Laborator 03 - Zaklady pocitacove grafiky - IZG
 * ibehun@fit.vutbr.cz
 *
 * $Id:$
 *
 * Popis: Hlavicky funkci pro funkce studentu
 *
 * Opravy a modifikace:
 * - 
 */

#ifndef Student_H
#define Student_H

/*****************************************************************************
 * Includes
 */
#include "color.h"

/*****************************************************************************
 * Ukoly pro cviceni
 */

/* Funkce vlozi pixel na pozici x, y. Pozor: nehlida velkost frame_bufferu, pokud 
 je dana souradnice mimo hranice. */
void putPixel(int x, int y, S_RGBA color);

/* Funkce vraci pixel z pozice x, y. Pozor: nehlida velkost frame_bufferu, pokud 
 je dana souradnice mimo hranice. */
S_RGBA getPixel(int x, int y);

/* Funkce pro vykresleni usecky se suradnicemi [x1,y1] [x2,y2] */
void drawLine (int x1, int y1, int x2, int y2, S_RGBA col);

void floodFill(int x, int y, S_RGBA color);
void invertFill(const S_Point * points, const int size,  const S_RGBA & color1, const S_RGBA & color2);

#endif /* Student_H */

/*****************************************************************************/
/*****************************************************************************/