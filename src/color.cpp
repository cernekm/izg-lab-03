/******************************************************************************
 * Laborator 03 - Zaklady pocitacove grafiky - IZG
 * ibehun@fit.vutbr.cz
 *
 * $Id:$

 * Popis:
 *    soubor obsahuje definice barevnych datovych typu + pomocne funkce pro
 *    praci s nimi 
 *
 * Opravy a modifikace:
 * -
 */

#include "color.h"

/****************************************************************************
 * Globalni promenne
 */

/* Preddefinovane barvy */
const S_RGBA    COLOR_BLACK = { 0, 0, 0, 255 };
const S_RGBA    COLOR_GREEN = { 0, 255, 0, 255 };
const S_RGBA    COLOR_BLUE  = { 255, 0, 0, 255 };
const S_RGBA    COLOR_RED   = { 0, 0, 255, 255 };
const S_RGBA    COLOR_WHITE = { 255, 255, 255, 255 };


/*****************************************************************************/
/*****************************************************************************/
