/******************************************************************************
 * Laborator 03 - Zaklady pocitacove grafiky - IZG
 * ibehun@fit.vutbr.cz
 *
 * $Id:$

 * Popis:
 *    soubor obsahuje definice barevnych datovych typu + pomocne funkce pro
 *    praci s nimi 
 *
 * Opravy a modifikace:
 * -
 */

#ifndef Color_H
#define Color_H

/*****************************************************************************
 * Includes
 */

#include "base.h"


/*****************************************************************************
 * Definice typu a fci pro reprezentaci RGBA barvy
 */

/* Struktura reprezentujici RGBA barvu (interne reprezentovana jako BGRA) */
typedef struct S_RGBA
{
    unsigned char   blue, green, red, alpha;

	static bool compare(const S_RGBA &c1, const S_RGBA &c2)
	{
		return (
			c1.red == c2.red && c1.green == c2.green &&
			c1.blue == c2.blue && c1.alpha == c2.alpha
		);
	}

	static S_RGBA interpolate(const S_RGBA &c1, const S_RGBA &c2, float factor)
	{
		S_RGBA c;
		c.red   = (char) ((1 - factor) * c1.red   + factor * c2.red);
		c.green = (char) ((1 - factor) * c1.green + factor * c2.green);
		c.blue  = (char) ((1 - factor) * c1.blue  + factor * c2.blue);
		return c;
	}
} S_RGBA;

IZG_INLINE bool operator==(const S_RGBA &c1, const S_RGBA &c2)
{
	return S_RGBA::compare(c1, c2);
}

IZG_INLINE bool operator!=(const S_RGBA &c1, const S_RGBA &c2)
{
	return !S_RGBA::compare(c1, c2);
}

/* Vytvori cernou barvu */
IZG_INLINE S_RGBA makeBlackColor()
{
    S_RGBA color;
    color.red = color.green = color.blue = 0;
    color.alpha = 255;
    return color;
}

/* Vytvori barvu o zadanych hodnotach */
IZG_INLINE S_RGBA makeColor(unsigned char r, unsigned char g, unsigned char b)
{
    S_RGBA color;
    color.red = r;
    color.green = g;
    color.blue = b;
    color.alpha = 255;
    return color;
}

/* Konstanty barev pouzivane v programu */
extern const S_RGBA     COLOR_BLACK;
extern const S_RGBA     COLOR_GREEN;
extern const S_RGBA     COLOR_BLUE;
extern const S_RGBA     COLOR_RED;
extern const S_RGBA     COLOR_WHITE;


#endif // Color_H

/*****************************************************************************/
/*****************************************************************************/
