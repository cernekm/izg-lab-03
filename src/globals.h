/******************************************************************************
 * Laborator 03 - Zaklady pocitacove grafiky - IZG
 * ibehun@fit.vutbr.cz
 *
 * $Id:$
 *
 * Popis: Soubor obsahuje globalni promenne pro toto cviceni
 *
 * Opravy a modifikace:
 * -
 */

#ifndef Globals_H
#define Globals_H

/*****************************************************************************
 * Includes
 */

#include "SDL/SDL.h"

/*****************************************************************************
 * Globalni promenne (definovane v main.c)
 */

/* kreslici buffer knihovny SDL */
extern SDL_Surface			* screen;

/* kreslici buffer IZG cvi�en� */
extern S_RGBA				* frame_buffer;

/* pomocna promenna pro ukonceni aplikace */
extern int					width;
extern int					height;

#endif /* Globals_H */
/*****************************************************************************/
/*****************************************************************************/