///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/******************************************************************************
 * Projekt - Zaklady pocitacove grafiky - IZG Lab 03
 * ibehun@fit.vutbr.cz
 *
 * $Id: $
 *
 * *) Ovladani programu:
 *      "Leva mys"		- Vypis hodnoty pixelu
 *      "Esc"			- ukonceni programu
 *      "C"             - Vymaze frame buffer
 *      "L"				- Nahrani zkusebniho obrazku image.bmp
 *      "S"				- Ulozeni obrazovky do out.bmp
 *      "R"				- Cervena barva
 *      "G"				- Zelena barva
 *      "B"				- Modra barva
 *      "F"				- Flood fill
 *      "I"				- Invert fill
 *
 * Opravy a modifikace:
 * - 
 */

/******************************************************************************
******************************************************************************
 * Includes
 */

#ifdef _WIN32
    #include <windows.h>
#endif

/* nase veci... */
#include "color.h"
#include "student.h"
#include "io.h"
#include "globals.h"

/* knihovna SDL + system */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>


/******************************************************************************
 ******************************************************************************
 * Globalni konstanty a promenne
 */

/* titulek hlavniho okna */
const char     * PROGRAM_TITLE  = "IZG/2013 Lab 03";

/* defaultni velikost okna */
const int          DEFAULT_WIDTH    = 800;
const int          DEFAULT_HEIGHT   = 600;

/* kreslici buffer knihovny SDL */
SDL_Surface         * screen        = NULL;

/* kreslici buffer IZG cvi�en� */
S_RGBA				* frame_buffer	= NULL;

/* pomocna promenna pro ukonceni aplikace */
int                 quit            = 0;
int					width			= 800;
int					height			= 600;

/* Nazvy i/o souboru */
char *				input_image		= "data/scooby.bmp";
char *				output_image	= "data/out.bmp";

S_RGBA               color1         = COLOR_GREEN;
S_RGBA               color2         = COLOR_BLUE;
SeedStack            points;
// Stav pouziti algoritmu
UseAlg algorithm;
bool isOld = false;

/******************************************************************************
 ******************************************************************************

	Testy, jake budou pouzity i pri kontrole ukolu. 
    Pokud vase reseni projde timto, tak projde i pri kontrole. Zmena bude
    v poctu bodu, jejich souradnicich a pouzitych barvach. Testovane jevy
    zustavaji...
    
    UPOZORNENI: 
    Testy budou spousteny postupne a v tom poradi, v jakem jsou zde. 
    Pokud chcete nejake body a vite, ze vas algoritmus nezvladne vsechno dobre,
    tak se snazte, aby alespon nespadl a dokoncil testy vsechny.
    
    Omluvte pouzitou czenglish v komentarich...
*/

// Vstupni souradnice
const int points1[4][2] = {{10,10},{128,10},{128,67},{10,67}};
const int points2[3][2] = {{50,50},{150,50},{100,200}};
const int points31[8][2] = {{0,0},{128,128},{97,16},{15,200},{2,22},{232,86},{6,137},{68,68}};
const int points41[4][2] = {{120, 60}, {33, 150}, {86, 180}, {210, 53}};
const int points5[8][2] = {{160, 160}, {80, 40}, {100, 100},{50,30},{211,37},{144,13},{213,61}, {111,48}};
const int points7[2][2] = {{0,0},{128,128}};
const int points8[16][2] = {{0,0},{128,128},{128,128},{0,0},{2,22},{232,86},{6,137},{68,68},{212,137},{15,91},{97,3},{111,48},{11,3},{211,37},{144,13},{213,61}};
const int points9[4][2] = {{120, 200}, {300, 120},{101, 200},{100, 50}};
const int points10[4][2] = {{10,10},{128,10},{10,67},{30,167}};

// Vytvor pole bodu
S_Point * makeSeedStack(const int points[][2], int length)
{

	S_Point * s = new S_Point[ length ];

	for(int i = 0; i < length; i++)
		s[ i ] = S_Point(points[i][0], points[i][1]);

	return s;
}

// spust jeden z testu
void runTest(int test)
{
	S_Point * points(NULL);

	switch(test)
	{
		//-----------------------------
		// Simple tests - solid fill

		// 1) Try to create filled rectangle
	case 1:
		points = makeSeedStack(points1, 4);
		invertFill( points, 4, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 2) Try to create polygon
	case 2:
		points = makeSeedStack(points2, 3);
		invertFill( points, 3, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 3) Create polygon over polygon
	case 3:
		points = makeSeedStack(points31, 8);
		invertFill( points, 8, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		//------------------------------
		// Stress tests

		// 4) Try to create polygons out of view
	case 4:
		points = makeSeedStack(points41, 4);
		invertFill( points, 4, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 5) Try to create polygon partialy out of view
	case 5:
		points = makeSeedStack(points5, 8);
		invertFill( points, 8, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 6) Try to create wrong polygon - zero points
	case 6:
		points = new S_Point[0];
		invertFill( points, 0, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 7) Try to create wrong polygon - two points. Line must be drawn.
	case 7:
		points = makeSeedStack(points7, 2);
		invertFill( points, 2, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 8) Try to create partialy not filled polygon 
	case 8:
		points = makeSeedStack(points8, 16);
		invertFill( points, 16, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 9) Try to create really big polygon
	case 9:
		points = makeSeedStack(points9, 4);
		invertFill( points, 4, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;

		// 10) Try to create polygon with zero length side
	case 10:
		points = makeSeedStack(points10, 4);
		invertFill( points, 4, COLOR_GREEN, COLOR_RED);
		delete [] points;
		break;
	}


}  // void runTest()

// Copy bitmap 
// x, y - destination position
// dx1 - source width
// dy1 - source height
// dx2 - destination width

void copyBuffer(S_RGBA * src, S_RGBA * dst, int x, int y, int dx1, int dy1, int dx2 )
{
	// Prepocitana pozice v cilovem bufferu;
	S_RGBA * dstP(dst + x + y * dx2);

	// castecna kopie obsahu frame bufferu
	for( int i = 0; i < dy1; i++ )
		memcpy( dstP + i*dx2, src + i*dx1, dx1*sizeof(S_RGBA) );
}

// run all tests and store them to the tiled bitmap
void runAllMakeTiled()
{
	int fw = 5 * width;
	int fh = 2 * height;

	// alokace pameti 
	S_RGBA * tiled = new S_RGBA[fw * fh];
	memset( tiled, 0, fw*fh*sizeof(S_RGBA) );

	// vsechny testy
	for( int y = 0; y < 2; ++y)
		for( int x = 0; x < 5; ++x)
		{
			// Resolve test number
			int test = 1 + y*5+x;

			// Clear frame buffer
			memset( frame_buffer, 0, height*width*sizeof( S_RGBA ));

			// Run test
			runTest(test);

			// Copy bitmap
			copyBuffer( frame_buffer, tiled, x * width, y * height, width, height, fw );

		}

		// Clear frame buffer
		memset( frame_buffer, 0, height*width*sizeof( S_RGBA ));

		// save bitmap
		saveMyBitmap( "tiled.bmp", &tiled, fw, fh );

		// Dealokace pameti
		delete [] tiled;
}


/******************************************************************************
 ******************************************************************************
 * funkce zajistujici prekresleni obsahu okna programu
 */

void onDraw(void)
{
    /* Test existence frame bufferu a obrazove pameti */
	IZG_ASSERT(frame_buffer && screen);

    /* Test typu pixelu */
    IZG_ASSERT(screen->format->BytesPerPixel == 4);

    /* Kopie bufferu do obrazove pameti */
    SDL_LockSurface(screen);

	/* Test, pokud kopirujeme rozdilnou velikost frame_bufferu a rozdilne pameti, musime pamet prealokovat */
	if (width != screen->w || height != screen->h)
	{
		SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);
		/*SDL_FreeSurface(screen);
		if (!(screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE|SDL_ANYFORMAT)))
		{
			IZG_ERROR("Cannot realocate screen buffer");
			SDL_Quit();
		}*/
	}
	MEMCOPY(screen->pixels, frame_buffer, sizeof(S_RGBA) * width * height);
    SDL_UnlockSurface(screen);
	
    /* Vymena zobrazovaneho a zapisovaneho bufferu */
    SDL_Flip(screen);
}


/******************************************************************************
 ******************************************************************************
 * funkce reagujici na stisknuti klavesnice
 * key - udalost klavesnice
 */

void onKeyboard(SDL_KeyboardEvent *key)
{
    /* test existence rendereru */
	IZG_ASSERT(frame_buffer);

    /* vetveni podle stisknute klavesy */
    switch( key->keysym.sym )
    {
		case SDLK_c:
			memset(frame_buffer, 0, width*height*sizeof(S_RGBA));
			break;
		case SDLK_r:
			if (key->keysym.mod & KMOD_LCTRL)
				color2 = COLOR_RED;
			else
				color1 = COLOR_RED;
			break;
		case SDLK_g:
			if (key->keysym.mod & KMOD_LCTRL)
				color2 = COLOR_GREEN;
			else
				color1 = COLOR_GREEN;
			break;
		case SDLK_b:
			if (key->keysym.mod & KMOD_LCTRL)
				color2 = COLOR_BLUE;
			else
				color1 = COLOR_BLUE;
			break;
		case SDLK_w:
			if (key->keysym.mod & KMOD_LCTRL)
				color2 = COLOR_WHITE;
			else
				color1 = COLOR_WHITE;
			break;
		case SDLK_k:
			if (key->keysym.mod & KMOD_LCTRL)
				color2 = COLOR_BLACK;
			else
				color1 = COLOR_BLACK;
			break;
		case SDLK_f:
			algorithm = USE_FLOOD_FILL;
			break;
		case SDLK_i:
			algorithm = USE_SCANLINE_FILL;
			points.erase(points.begin(), points.end());
			break;
		case SDLK_0:
		case SDLK_1:
		case SDLK_2:
		case SDLK_3:
		case SDLK_4:
		case SDLK_5:
		case SDLK_6:
		case SDLK_7:
		case SDLK_8:
		case SDLK_9:
			runTest(key->keysym.sym - SDLK_0 + 1);
			break;
        /* ukonceni programu - klavesa Esc*/
        case SDLK_ESCAPE:
            quit = 1;
            break;

		case SDLK_l:
			/* Stisknuti L tlacitka nahraje obrazek */
			if (loadMyBitmap(input_image, &frame_buffer, &width, &height))
				IZG_INFO("File successfully loaded\n")
			else
				IZG_ERROR("Error in loading the file.\n");
            break;
		
		case SDLK_s:
			/* Stisknuti S tlacitka ulozi obrazek */
			if (saveMyBitmap(output_image, &frame_buffer, width, height))
				IZG_INFO("File successfully saved\n")
			else
				IZG_ERROR("Error in saving the file.\n");
            break;
        default:
            break;
    }
}

/******************************************************************************
 ******************************************************************************
 * funkce reagujici na zmacknuti tlacitka mysi
 * mouse - udalost mysi
 */

void onMouseDown(SDL_MouseButtonEvent *mouse)
{
    if( mouse->button == SDL_BUTTON_LEFT)
    {
		switch(algorithm){
			case USE_SCANLINE_FILL:
				putPixel(mouse->x, mouse->y, color1);
				points.push_back(S_Point(mouse->x, mouse->y));
				isOld = true;
				break;
			case USE_FLOOD_FILL:
				floodFill(mouse->x, mouse->y, color1);		
				break;
		}
    }
	if( mouse->button == SDL_BUTTON_RIGHT) {
		if(algorithm == USE_SCANLINE_FILL && isOld)
		{
			S_Point * pointa;
			stackToArray( points, pointa );
			// Fill polygon
			invertFill(pointa, (const int) points.size(),  color1, color2);
			delete [] pointa;
			points.clear();
			isOld = false;
		}
	}

}

/******************************************************************************
 ******************************************************************************
 * funkce tiskne napovedu na stdout
 */
void printHelpText()
{
	IZG_INFO("Application loaded - IZG LAB 03 - 2D area filling. Controls:\n\n"
			 "Left mouse click: \n"
			 "    Applies selected algorithm: flood or invert fill\n\n"
			 "Right mouse click:\n"
			 "    Adds new point to polygon\n\n"
			 "L key:\n"
			 "    Loads the testing image.bmp image\n"
			 "       (depends on PutPixel function)\n\n"
			 "S key:\n"
			 "    Saves current view into out.bmp image\n"
			 "        (depends on GetPixel function)\n\n"
			 "C key:\n"
			 "    Clears framebuffer\n\n"
			 "0..9 key:\n"
			 "    Run test o..9\n\n"
			 "F key:\n"
			 "    Sets algorithm to flood fill\n\n"
			 "I key:\n"
			 "    Sets algorithm to invert fill\n\n"
			 "R key:\n"
			 "    Sets first color to red \n\n"
			 "G key:\n"
			 "    Sets first color to green \n\n"
			 "B key:\n"
			 "    Sets first color to blue \n\n"
			 "W key:\n"
			 "    Sets first color to white \n\n"
			 "K key:\n"
			 "    Sets first color to black \n\n"
			 "CTRL + [RGBWK] key:\n"
			 "    Sets second color\n\n"

			 "========================================================================\n\n")
}

/******************************************************************************
 ******************************************************************************
 * hlavni funkce programu
 * argc - pocet vstupnich parametru
 * argv - pole vstupnich parametru
 */

int main(int argc, char *argv[])
{
    SDL_Event event;
	
	/* Inicializace SDL knihovny */
    if( SDL_Init(SDL_INIT_VIDEO) == -1 )
    {
        IZG_SDL_ERROR("Could not initialize SDL library");
    }

    /* Nastaveni okna */
    SDL_WM_SetCaption(PROGRAM_TITLE, 0);
   
	/* Alokace frame bufferu (okno + SW zapisovaci */
	if (!(screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE)))
    {
        SDL_Quit();
        return 1;
    }

	if (!(frame_buffer = (S_RGBA *)malloc(sizeof(S_RGBA) * width * height)))
	{
		SDL_Quit();
        return 1;
	}

	printHelpText();

	/* Kreslime, dokud nenarazime na SDL_QUIT event */
	while(!quit) 
    {
        /* Reakce na udalost */
        while( SDL_PollEvent(&event) )
        {
            switch( event.type )
            {
                /* Udalost klavesnice */
                case SDL_KEYDOWN:
                    onKeyboard(&event.key);
                    break;

                /* Udalost mysi */
				case SDL_MOUSEBUTTONDOWN:
                    onMouseDown(&event.button);
                    break;

                /* SDL_QUIT event */
                case SDL_QUIT:
                    quit = 1;
                    break;

                default:
                    break;
            }
        }

		/* Provedeme preklopeni zapisovaciho frame_bufferu na obrazovku*/
		onDraw();
    }

	/* Uvolneni pameti */
	SDL_FreeSurface(screen);
	free(frame_buffer);
    SDL_Quit();
  
	IZG_INFO("Bye bye....\n\n");
    return 0;
}


/*****************************************************************************/
/*****************************************************************************/
